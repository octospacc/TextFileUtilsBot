# Text File Utils Bot
Telegram Bot capable of useful stuff with text and files.

Try it directly by contacting [@TextFileUtilsBot](https://t.me/TextFileUtilsBot) on Telegram!

### Features
- Get a .txt file from a text message, or from the caption of a media
