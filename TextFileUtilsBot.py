#!/usr/bin/python3

# -
# | Text File Utils Bot
# | Telegram Bot capable of useful stuff with text and files
# |
# | Copyright (C) 2021, OctoSpacc
# | Licensed under the AGPLv3
# -

import json
from io import StringIO
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

Config = {}

def LoadConfig():
	try:
		global Config

		with open("Config.json", "r") as ConfigFile:
			Config = json.load(ConfigFile)

	except FileNotFoundError:
		print("[E] Couldn't load Config.json, it doesn't exist. Exiting.")
		exit()
	except JSONDecodeError:
		print("[E] Couldn't decode Config.json, it is invalid. Exiting.")
		exit()
	except Exception:
		print("[E] Unknown error loading Config.json. Exiting.")
		exit()

def ValidateConfig():
	if type(Config["Telegram Bot Token"]) != str or ":" not in Config["Telegram Bot Token"]:
		print("[E] The Telegram Bot Token in the Config.json file is invalid. Exiting.")
		exit()

def ShortFileName(Text):
	if len(str(Text)) > 32:
		return str(Text)[:31] + "[...].txt"
	else:
		return str(Text) + ".txt"

def CommandStart(update, context):
	if type(Config["Info / Source Code Link"]) != str or Config["Info / Source Code Link"] == "":
		Config["Info / Source Code Link"] = "https://gitlab.com/octospacc/TextFileUtilsBot"

	update.message.reply_text("👋 Hello! I can do useful stuff related to text and files.\n\nYou can either:\n- Send me a message ✍️ starting with /txt and containing some other text\n- Reply ⤴️ with /txt to a text message or a message with a caption\nI will send a .txt file 📂 back to you, which will contain your text.\n\nThis bot is free software!\nℹ️ More info and Source Code:\n" + str(Config["Info / Source Code Link"]), reply_to_message_id=update.message.message_id)

def CommandTxt(update, context):
	if update.message.reply_to_message != None:
		if update.message.reply_to_message.text != None:
			update.message.reply_document(StringIO(update.message.reply_to_message.text), filename=ShortFileName(update.message.reply_to_message.text), reply_to_message_id=update.message.message_id)	
		elif update.message.reply_to_message.caption != None:
			update.message.reply_document(StringIO(update.message.reply_to_message.caption), filename=ShortFileName(update.message.reply_to_message.caption), reply_to_message_id=update.message.message_id)	
		else:
			update.message.reply_text("❌ Please reply ⤴️ with /txt to a text message or a message with a caption, or write ✍️ in a single message /txt followed by some text!", reply_to_message_id=update.message.message_id)

	else:
		if len(update.message.text) < 6:
			update.message.reply_text("❌ Please write ✍️ some text in your message after /txt, or write /txt as a reply ⤴️ to a text message or a message with a caption!", reply_to_message_id=update.message.message_id)
		else:
			update.message.reply_document(StringIO(update.message.text[5:]), filename=ShortFileName(update.message.text[5:]), reply_to_message_id=update.message.message_id)

def Main():
	LoadConfig()
	ValidateConfig()
	
	Bot = Updater(Config["Telegram Bot Token"], use_context=True)
	print("[I] TextFileUtilsBot started!")

	Bot.dispatcher.add_handler(CommandHandler("start", CommandStart))
	Bot.dispatcher.add_handler(CommandHandler("txt", CommandTxt))

	Bot.start_polling()

if __name__ == "__main__":
	try:
		Main()
	except KeyboardInterrupt:
		print("[I] KeyboardInterrupt received, exiting.")
		exit()